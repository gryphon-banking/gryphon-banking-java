package com.gryphon.banking.repository;

import org.springframework.data.repository.CrudRepository;

import com.gryphon.banking.domain.user.User;

/**
 * Service responsible for persisting {@link User} objects.
 * 
 * @author Justin Albano <albano.justin@gmail.com>
 */
public interface UserRepository extends CrudRepository<User, String> {
}

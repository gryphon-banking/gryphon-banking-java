package com.gryphon.banking.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gryphon.banking.domain.account.Account;

/**
 * Service responsible for persisting {@link Account} objects.
 * 
 * @author Justin Albano <albano.justin@gmail.com>
 */
public interface AccountRepository extends CrudRepository<Account, String> {

    /**
     * Finds all accounts owned by the supplied user.
     * 
     * @param userId
     *            The ID of the desired owner.
     * @return
     *             A list of all accounts owned by the supplied user.
     */
    public List<Account> findAllOwnedBy(String userId);
}

package com.gryphon.banking.repository.mongodb.convert;

import javax.money.MonetaryAmount;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

@WritingConverter
public class MonetaryAmountWriteConverter implements Converter<MonetaryAmount, String> {

    @Override
    public String convert(MonetaryAmount amount) {
        return amount.toString();
    }
}

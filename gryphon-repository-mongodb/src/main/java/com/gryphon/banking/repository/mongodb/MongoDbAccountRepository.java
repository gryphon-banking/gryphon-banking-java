package com.gryphon.banking.repository.mongodb;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.gryphon.banking.domain.account.Account;
import com.gryphon.banking.repository.AccountRepository;

@Repository
public interface MongoDbAccountRepository extends MongoRepository<Account, String>, AccountRepository {

    @Override
    @Query("{ 'owner' : ?0 }")
    public List<Account> findAllOwnedBy(String userId);

}

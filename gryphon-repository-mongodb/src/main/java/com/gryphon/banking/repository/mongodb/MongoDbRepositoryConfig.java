package com.gryphon.banking.repository.mongodb;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.gryphon.banking.repository.mongodb.convert.MonetaryAmountReadConverter;

@Configuration
@EnableMongoRepositories
@ComponentScan(basePackageClasses = MonetaryAmountReadConverter.class)
public class MongoDbRepositoryConfig {
}

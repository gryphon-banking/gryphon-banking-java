package com.gryphon.banking.repository.mongodb.convert;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class MonetaryAmountReadConverter implements Converter<String, MonetaryAmount> {

    @Override
    public MonetaryAmount convert(String amount) {
        
        String[] components = amount.split(" ");
        
        CurrencyUnit unit = Monetary.getCurrency(components[0]);
        
        return Monetary.getDefaultAmountFactory()
            .setCurrency(unit)
            .setNumber(Double.parseDouble(components[1]))
            .create();
    }

}

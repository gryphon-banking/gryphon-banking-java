package com.gryphon.banking.repository.mongodb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import com.gryphon.banking.repository.mongodb.convert.MonetaryAmountReadConverter;
import com.gryphon.banking.repository.mongodb.convert.MonetaryAmountWriteConverter;
import com.mongodb.MongoClient;

import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;

@Configuration
@SuppressWarnings("deprecation")
public class MockMongoDbConfig extends AbstractMongoConfiguration {

    private static final String MONGO_DB_URL = "localhost";
    private static final String MONGO_DB_NAME = "embeded_db";
    
    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), MONGO_DB_NAME);
    }

    @Override
    public MongoClient mongoClient() {
        
        try {
            EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
            mongo.setBindIp(MONGO_DB_URL);
            return mongo.getObject();
        } 
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected String getDatabaseName() {
        return MONGO_DB_NAME;
    }

    @Override
    public CustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new MonetaryAmountReadConverter());
        converters.add(new MonetaryAmountWriteConverter());
        return new MongoCustomConversions(converters);
    }
}

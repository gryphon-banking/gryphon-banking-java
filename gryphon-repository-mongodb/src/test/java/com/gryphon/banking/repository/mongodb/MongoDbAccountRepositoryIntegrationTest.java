package com.gryphon.banking.repository.mongodb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gryphon.banking.domain.account.Account;
import com.gryphon.banking.domain.account.Transaction;
import com.gryphon.banking.domain.money.Money;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {
    MongoDbRepositoryConfig.class,
    MockMongoDbConfig.class
})
public class MongoDbAccountRepositoryIntegrationTest {
    
    @Autowired
    private MongoConverter converter;

    @Autowired
    private MongoDbAccountRepository repository;
    
    @BeforeEach
    public void setUp() {
        repository.deleteAll();
    }
    
    @Test
    public void givenNoAccounts_WhenFindAll_ThenNoAccountsFound() {
        assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void givenOneAccount_WhenFindAll_ThenOneAccountFound() {
        
        Account account = new Account("foo", "bar");
        
        repository.save(account);
        
        List<Account> foundAccounts = repository.findAll();
        
        assertEquals(1, foundAccounts.size());
        assertContainsAccountWithId(foundAccounts, account.getId());
    }
    
    private static void assertContainsAccountWithId(List<Account> accounts, String id) {
        
        boolean containsAccount = accounts.stream()
            .anyMatch(account -> Objects.equals(account.getId(), id));
        
        assertTrue(containsAccount, "Account list does not contain account with ID " + id);
    }

    @Test
    public void givenSameAccountSavedTwice_WhenFindAll_ThenOneAccountFound() {
        
        Account account1 = new Account("foo", "bar");
        Account account2 = new Account("foo", "baz");
        
        repository.save(account1);
        repository.save(account2);
        
        List<Account> foundAccounts = repository.findAll();
        
        assertEquals(1, foundAccounts.size());
        assertContainsAccountWithId(foundAccounts, account2.getId());
        assertEquals(account2.getName(), foundAccounts.get(0).getName());
    }

    @Test
    @Disabled
    public void givenOneAccountSavedWithTransations_WhenFindAll_ThenOneAccountFoundWithSameTransactions() {
        
        Account account = new Account("foo", "bar");
        Transaction transaction = new Transaction("trans1", Money.dollars(1), Instant.now());
//        account.addTransaction(transaction);
        
        System.out.println(converter.convertToMongoType(transaction.getAmount()));
        
        repository.save(account);
        
        List<Account> foundAccounts = repository.findAll();
        
        assertEquals(1, foundAccounts.size());
        assertIncludesTransaction(foundAccounts.get(0), transaction);
    }
    
    private static void assertIncludesTransaction(Account account, Transaction transaction) {
        
        boolean transactionFround = account.getTransactions()
            .stream()
            .anyMatch(t -> t.equals(transaction));
        
        assertTrue(transactionFround, "Account does not contain transaction with ID " + transaction.getId());
    }
}
